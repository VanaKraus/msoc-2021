# machine_learning

This module uses ```tensorflow (2.6.0)``` python package (can be obtained via pip, for futher information see the official [TensorFlow website](https://www.tensorflow.org/install/pip)). 

If you want to use this module stand-alone, you may want to consider downloading the ```machine_learning_launcher.py``` from the main directory.

To increase the neural network performance it is recommended to edit __build_model() function. 

## Limitation

This module requires vectorizing AST string for the neural network input. This text vectorization is not working at the moment. Before any use in a project it is thus necessary to edit the text vectorization. We are currently working on fixing this problem.
