
# Copyright (C) 2021, Krystof Matejka, Ivan Kraus
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import numpy as np
import tensorflow as tf
from tensorflow import keras

from tensorflow.keras.layers import TextVectorization
from tensorflow.keras import layers
from tensorflow.python.framework.ops import Tensor

#batch size of data (used in model.fit())
__batch_size:int = 1
#training epochs (used in model.fit())
__epochs:int = 1
#checkpoint name (used for loading and saving checkpoints)
__checkpoint_name:str = "cp.ckpt"

#TODO Text vectorization (returns unconstant tensor shape)
def __array_string_to_data(array_string):
    """Converts a list/array of strings into vectorized tensor.

    Args:
        array_string (list/array of strings): List/array of strings that is converted.

    Returns:
        Tensor: Proccesed tensor data for a neural network dataset.
    """
    string_data = np.array(array_string)
    vectorizer = TextVectorization(output_mode="int")
    vectorizer.adapt(string_data)
    return vectorizer(string_data)

def __create_train_data(string_samples, boolean_labels):
    """Creates a dataset for a neural network

    Args:
        string_samples (list/array of strings): List/array of strings that are used as samples.
        boolean_labels (list/array of booleans): List/array of booleans that are used as labels.

    Returns:
        two lists/arrays: Returns a list/array of samples (int) and labels (boolean).
    """
    samples_array = []
    for sample in string_samples:
        tmp = []
        tmp.append(sample)
        samples_array.append(__array_string_to_data(tmp)[0])

    samples = np.array(samples_array)
    labels = np.array(boolean_labels)
    return samples, labels


def __build_model():
    """Creates a tensor flow model. Can be edited for specific model use.

    Returns:
        tensorflow.keras.Model: Builded and compiled tensor flow model. 
    """
    #TODO Shape (+ Text Vectorization)
    inputs = keras.Input(shape=(5))
    x = layers.Dense(3, activation="relu")(inputs)
    x = layers.Dense(3, activation="relu")(x)
    outputs = layers.Dense(1, activation="softmax")(x)
    model = keras.Model(inputs, outputs) 
    #compile model
    model.compile(loss='categorical_crossentropy', optimizer='adam')
    #model.summary() #prints model summery
    return model



def train_model(string_samples, boolean_labels):
    """Creates and trains a tensorflow model.

    Args:
        string_samples (list/array of strings): List/array of strings that are used as trianing samples.
        boolean_labels (list/array of booleans): List/array of booleans that are used as training labels.

    Returns:
        tensorflow.keras.Model: Trained tensor flow model. 
    """
    x_train, y_train = __create_train_data(string_samples, boolean_labels)
    model = __build_model()
    

    model.fit(x_train, y_train, batch_size=__batch_size, epochs=__epochs)
    return model

def save_model(string_samples, boolean_labels, save_dir_path):
    """Creates, trains and saves a tensorflow model.

    Args:
        string_samples (list/array of strings): List/array of strings that are used as trianing samples.
        boolean_labels (list/array of booleans): List/array of booleans that are used as training labels.
        save_dir_path (str): A directory path where the model checkpoints will be saved.

    Returns:
        tensorflow.keras.Model: Trained tensor flow model. 
    """
    checkpoint_path = os.path.join(save_dir_path, __checkpoint_name)

    cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,
                                                    save_weights_only=True,
                                                    verbose=1)
    
    x_train, y_train = __create_train_data(string_samples, boolean_labels)
    model = __build_model()

    model.fit(x_train, y_train, batch_size=__batch_size, epochs=__epochs, callbacks=[cp_callback])
    return model

def load_model(load_dir_path):
    """Loades an existing trained neurral network model (defined in __build_model()) based on checkpoints.

    Args:
        load_dir_path (str): A directory path where the model checkpoints are saved.

    Returns:
        tensorflow.keras.Model: Trained tensor flow model. 
    """
    checkpoint_path = os.path.join(load_dir_path, __checkpoint_name)
    model = __build_model()
    model.load_weights(checkpoint_path)
    return model

def test_model(string_samples, boolean_labels, model):
    """Tests a trained tensorflow model.

    Args:
        string_samples (list/array of strings): list/array of strings that are used as testing samples.
        boolean_labels (list/array of booleans): List/array of booleans that are used as testing labels.
        model ([tensorflow.keras.Model): Trained tensor flow model that is being tested. 

    Returns:
        (int, int): Returns the loss value & metrics values for the model in test mode.
    """
    x_test, y_test = __create_train_data(string_samples, boolean_labels)
    return model.evaluate(x_test, y_test)

def predict_model(string_sample, model):
    """Predicts an output of a string_sample based on the trained model.

    Args:
        string_sample (str): Input sample for predicting the label.
        model ([tensorflow.keras.Model): Trained tensor flow model that is being used. 

    Returns:
        int: Most probable predictions
    """
    probability_model = tf.keras.Sequential([model, tf.keras.layers.Softmax()])
    predictions = probability_model.predict(__array_string_to_data(string_sample).numpy())
    return np.argmax(predictions[0])
