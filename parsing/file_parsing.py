
# Copyright (C) 2021, Krystof Matejka, Ivan Kraus
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from abc import abstractmethod
import json
import os

from antlr4.tree.Tree import ParseTree, ParseTreeVisitor
import antlr_ast.ast as ast

import parsing.grammar as g


class parse_tree_filter:
    '''Used to filter nodes in a parse tree.
    
    Methods:
        @abstractmethod keep_node(node:ParseTree): determines, whether to keep a node of the parse tree, or not.
    '''

    @abstractmethod
    def keep_node(self, node:ParseTree) -> bool:
        '''Determines whether to keep node in its parse tree or not.
        
        Args:
            node (ParseTree): a parse tree node.

        Returns:
            bool: True if node should be kept, False if otherwise.
        '''

        raise NotImplementedError('Not implemented')

class __filter_visitor(ParseTreeVisitor):
    filter:parse_tree_filter

    def __init__(self, filter:parse_tree_filter) -> None:
        super().__init__()
        self.filter = filter

    def visitChildren(self, node):
        if not self.filter.keep_node(node):
            node.parentCtx.children.remove(node)

        return super().visitChildren(node)


class file_parsing_setup:
    '''Contains a setup for file parsing.
    
    Methods:
        get_parse_tree(source_file:str, filter:parse_tree_filter=None) -> ast.ParseTree: creates a parse tree based on the given parameters.
        get_string_parse_tree(source_file:str, filter:parse_tree_filter=None) -> str: creates a parse tree and turns it into string.

    Vars:
        use_grammar_dictionary (bool, optional): whether to look through parsing/grammars.json to match pre-defined grammars based on source file extensions. True by default.
        grammar_file (str): path to the grammar file used.
        grammar_start_rule (str): start rule of the grammar used.
        antlr4shellcommand (str): sets the shell command used for launching antlr4. 'antlr4' by default.
    '''
    
    use_grammar_dictionary:bool = True
    grammar_file:str = None
    grammar_start_rule:str = None
    antlr4shellcommand:str = 'antlr4'

    def __init__(self, use_grammar_dictionary:bool=True, grammar_file:str=None, grammar_start_rule:str=None, antlr4shellcommand:str='antlr4'):
        '''Initialises global variables.

        Args:
            use_grammar_dictionary (bool, optional): whether to first look through parsing/grammars.json to match pre-defined grammars based on source file extensions. Defaults to True.
            grammar_file (str): sets the value of grammar_file (path to a grammar file intended for parsing).
            grammar_start_rule (str, optional): sets the value of grammar_start_rule (start rule of the grammar file used). Defaults to None.
            antlr4shellcommand (str, optional): sets the shell command used for launching antlr4. Defaults to 'antlr4'.
        '''

        self.use_grammar_dictionary = use_grammar_dictionary
        self.grammar_file = grammar_file
        self.grammar_start_rule = grammar_start_rule
        self.antlr4shellcommand = antlr4shellcommand

    def __make_parse_tree(self, source_file:str) -> ast.ParseTree:
        with open(source_file, 'r') as inputstream:
            extension = os.path.splitext(source_file)[1].lstrip('.')

            grammar_specifications = {}

            if self.use_grammar_dictionary:
                try:
                    with open('parsing/grammars.json', 'r') as grammar_spec_file:
                        grammar_specifications_dict = json.load(grammar_spec_file)
                        if not extension in grammar_specifications_dict: 
                            print(f'Warning: .{extension} grammar file not specified in parsing/grammars.json')
                        else: grammar_specifications = grammar_specifications_dict[extension]
                except FileNotFoundError as e: print('Warning: parsing/grammars.json not found')
            
            grammar_file = grammar_specifications['grammar_file'] if 'grammar_file' in grammar_specifications else self.grammar_file
            grammar_start_rule = grammar_specifications['start_rule'] if 'start_rule' in grammar_specifications else self.grammar_start_rule

            if grammar_file == None: raise Exception('file_parsing_setup: grammar file not specified. It is possible to specify it in the constructor.')
            if grammar_start_rule == None: raise Exception('file_parsing_setup: grammar start rule not specified. It is possible to specify it in the constructor.')

            grammarscriptinfos = g.script_info.guess(grammar_file, generate=True, antlr4shellcommand=self.antlr4shellcommand)
            mgrammar = g.grammar(grammarscriptinfos)
            return ast.parse(mgrammar, inputstream.read(), grammar_start_rule)

    def __filter_parse_tree(self, parse_tree:ast.ParseTree, filter:parse_tree_filter):
        __filter_visitor(filter).visit(parse_tree)


    def get_parse_tree(self, source_file:str, filter:parse_tree_filter=None) -> ast.ParseTree:
        '''Creates a parse tree object.

        Creates a parse tree and filters it, if a filter is available.

        Args: 
            source_file (str): path to a source file.
            filter (parse_tree_filter, optional): filter used to filter the parse tree.

        Returns:
            ParseTree: a parse tree object
        '''

        parsetree = self.__make_parse_tree(source_file)

        if filter != None:
            self.__filter_parse_tree(parsetree, filter)

        return parsetree


    def get_string_parse_tree(self, source_file:str, filter:parse_tree_filter=None) -> str:
        '''Alias of get_parse_tree(source_file, filter).toStringTree()'''
        return self.get_parse_tree(source_file, filter).toStringTree()
