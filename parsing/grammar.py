
# Copyright (C) 2021, Krystof Matejka, Ivan Kraus
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

'''Contains generic loading of lexers, parsers etc. for them to be easily worked with.'''

from typing import Iterable, Type
from pydoc import locate
from importlib.machinery import SourceFileLoader
from enum import Enum
import re
import os
import subprocess
import shlex

import antlr4

class script_type(Enum):
    '''Enum of script types.
    
    Values:
        NONE = 0
        LEXER = 1
        PARSER = 2
    '''

    NONE = 0
    LEXER = 1
    PARSER = 2

class script_info:
    '''Information about an antlr4 generated python script.

    Variables:
        gname (str): name of the original antlr4 grammar
        scriptpath (str): path of the script
        stype (script_type): type of the script

    Methods:
        __init__(scriptpath:str, gname:str, stype:script_type): initialises the object
        @classmethod guess(gpath:str): returns a list of script_infos based on the name of a given grammar file
        @classmethod guessm(gpath:Iterable): returns a list of script_infos based on the name of given grammar files
    '''

    gname:str
    scriptpath:str
    stype:script_type

    def __init__(self, scriptpath:str, gname:str, stype:script_type):
        self.scriptpath = scriptpath
        self.gname = gname
        self.stype = stype

    @classmethod
    def guess(cls, gpath:str, generate:bool=False, antlr4shellcommand:str='antlr4') -> list:
        '''Guesses if a lexer, parser, or both are generated from a .g4 grammar file based on the grammar file's name.

        Files ending with 'Lexer.g4' are considered to be lexers, files ending with 'Parser.g4' are considered to be parsers, \
        files ending only with '.g4' are considered to be both.

        Args:
            gpath (str): path of the grammar file
            generate (bool, optional): when True, generates missing python files via shell
            antlr4shellcommand (bool, optional): shell command used for calling antlr4. Defaults to 'antlr4'.
        
        Returns:
            None: if gpath is not a .g4 file
            list: list of script_info objects
        '''

        if len(gpath) <= 3 or not gpath[-3:] == '.g4':
            print(f'Warning: {cls.__name__}.guess: {gpath} is not a .g4 file')
            return None

        lexersearch = re.search(r'([a-zA-Z0-9]+Lexer)\.g4', gpath)
        parsersearch = re.search(r'([a-zA-Z0-9]+Parser)\.g4', gpath)

        res = []

        if not lexersearch == None:
            res = [ script_info(f'{gpath[:-3]}.py', lexersearch.group(1), script_type.LEXER) ]
        elif not parsersearch == None:
            res = [ script_info(f'{gpath[:-3]}.py', parsersearch.group(1), script_type.PARSER) ]
        else:
            gname = re.search(r'([a-zA-Z0-9]+)\.g4', gpath).group(1)
            res = [
                script_info(f'{gpath[:-3]}Parser.py', f'{gname}Parser', script_type.PARSER),
                script_info(f'{gpath[:-3]}Lexer.py', f'{gname}Lexer', script_type.LEXER)
            ]

        if generate:
            generated = True
            for el in res:
                generated &= os.path.isfile(el.scriptpath)
            
            if not generated:
                process_grammar(gpath, antlr4shellcommand=antlr4shellcommand)
                    
        return res

    @classmethod
    def guessm(cls, gpaths:Iterable[str], generate:bool=False, antlr4shellcommand:str='antlr4') -> list:
        '''Calls script_info.guess for all items of gpaths.

        Args:
            gpaths (Iterable): paths of grammar files
            generate (bool, optional): when True, generates missing python files via shell
            antlr4shellcommand (bool, optional): shell command used for calling antlr4. Defaults to 'antlr4'.
        
        Returns:
            None: if all gpaths are not .g4 files
            list: list of script_info objects
        '''

        res = []
        for gpath in gpaths:
            r = cls.guess(gpath, generate=generate, antlr4shellcommand=antlr4shellcommand)
            if not r == None:
                res += r
        
        return res if len(res) > 0 else None

def process_grammar(gpath:str, antlr4shellcommand:str='antlr4'):
    '''Calls antlr4 via shell for grammar specified in gpath.
    
    Args:
        gpath (str): path of the .g4 grammar file of concern.

    Returns:
        bool: True on antlr4 return code 0, False otherwise
    '''

    if not os.path.isfile(gpath):
        raise FileNotFoundError(f'{__name__}.process_grammar: "{gpath}" is not a file')
    if not gpath[-3:] == '.g4':
        raise Exception(f'{__name__}.process_grammar: "{gpath}" is not a .g4 file')
    
    targetdir = os.path.split(gpath)[0]

    return subprocess.call(shlex.split(antlr4shellcommand) + ['-Dlanguage=Python3', '-no-listener', '-o', targetdir, gpath], shell=True) == 0
    
class grammar:
    '''Summarises all antlr4 python modules generated from a set of grammar.'''

    Lexer:Type[antlr4.Lexer] = None
    Parser:Type[antlr4.Parser] = None

    __grammartypeproperties:dict[script_type, str] = {
        script_type.LEXER: 'Lexer',
        script_type.PARSER: 'Parser'
    }

    def __init__(self, scriptinfos:Iterable[script_info]=None):
        '''Initialises grammar.
        
        Calls grammar.load_script on all given scriptinfos.

        Args:
            scriptinfos (Iterable of script_info, optional): all script infos to be loaded
        '''

        if not scriptinfos == None:
            for scriptinfo in scriptinfos:
                self.load_script(scriptinfo)

    def load_script(self, scriptinfo:script_info):
        '''Loads a python module based on script_info.'''
        module = SourceFileLoader(scriptinfo.gname, scriptinfo.scriptpath).load_module()
        
        prop = getattr(self, self.__grammartypeproperties[scriptinfo.stype])
        newcls = locate(f'{scriptinfo.gname}.{scriptinfo.gname}')

        if prop != None:
            print(f'Warning: grammar.load_script: overwriting {scriptinfo.stype.name} from {prop} to {newcls}')
        
        setattr(self, self.__grammartypeproperties[scriptinfo.stype], newcls)
