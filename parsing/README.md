# parsing

This module uses [ANTLR v4](https://www.antlr.org/) to parse source files. It thus requires ANTLR v4 to be installed for its full functionality (see *ANTLR v4*).

Python packages used:
* ```antlr-ast (0.8.1)```
* ```antlr4-python3-runtime (4.9.2)``` (4.7.2 comes with ```antlr-ast```, however 4.9.2 also seems to work fine)

## Quick start

0. Install the required python packages.
1. Import ```parsing.file_parsing```
2. Create your ```file_parsing_setup``` object
3. You are ready to go!

## ANTLR v4

ANTLR v4 is a great tool which uses its .g4 grammar files to declare grammar of a given language. These files however need to be processed in order to create callable ones (since this project is written in Python, Python callable files are required). This can be done using the following shell command: ```antlr4 -Dlanguage=Python3 grammar_file.g4```.

In order to work with these callable files, the ```antlr4-python3-runtime``` package needs to be installed.

### Do I need ANTLR v4 to use this package?

Not strictly. Provided you already have your grammar file processed (like the two we included in this project), you only need the Python package. For an unprocessed grammar file however, the ANTLR v4 installation is needed. This module calls ```antlr4 -Dlanguage=Python3 -no-listener grammar_file.g4``` when it gets the sense that the grammar file has not been processed. Note that the command by which to call ANTLR v4 can be changed by altering ```antlr4shellcommand``` parameters when calling this module.

## Grammar files

See [here](https://github.com/antlr/grammars-v4) for ANTLR v4 grammar files.

We have so far tested the provided grammars for C and Python3 (for Python). The start rules of those are:

Grammar    | Start rule
---------- | -------------------
C.g4       | ```compilationUnit```
Python3.g4 | ```file_input```

These grammar files (and their start rules) are also already pre-defined in ```grammars.json``` parameter file to streamline the setup process, so that the grammar file and the grammar start rule do not need to be specified during initialisation. You can opt not to use ```grammars.json``` by setting ```use_grammar_dictionary``` to ```False```.