
# Copyright (C) 2021, Krystof Matejka, Ivan Kraus
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import json

import GitAPI.gitlab_downloader as gitlab
import GitAPI.github_downloader as github

class GitAPIException(Exception):
    pass

def __list_params():
    for param in params:
        print(f'\t{param}: {params[param]}')

def __help(): 
    print('Enter a command name to execute the given command.')
    print('List of available commands:')
    for command in commands:
        print(f'\t{command}')
    
    print()
    print('To specify parameters, enter "--parameter=value" or "-parameter value" ("--parameter" for booleans). '
        'This will override values specified in parameters files.')
    print('Boolean parameters are false by default. Entering "--parameter" sets them to true.')
    print('See "list-params" for the list of parameters.')

    print()
    print('Example input: ~/GitAPI-launcher.py download-gitlab --gitlab_all_commits --until=2021-06-01')
    print('This launches the "download-gitlab" command and additionaly sets "gitlab_all_commits" to true and "until" to "2021-06-01".')

def __load_file(path):
    with open(path, 'r') as file:
        paramsfromfile = json.loads(file.read())
        for param in paramsfromfile:
            if not param in params:
                print(f'unknown parameter "{param}"')
                continue
            params[param] = paramsfromfile[param]
        file.close()

def __load_args():
    lastarg = ''
    for arg in sys.argv:
        if len(lastarg) > 0:
            if not lastarg in params:
                print(f'unknown parameter "{lastarg}"')
                continue

            params[lastarg] = arg
            lastarg = ''
            
        elif arg[0:2] == '--':
            splitted = arg[2:].split('=')

            if not splitted[0] in params:
                print(f'unknown parameter "{splitted[0]}"')
                continue

            if len(splitted) == 1:
                params[splitted[0]] = True
            elif len(splitted) >= 2:
                params[splitted[0]] = splitted[1]

        elif arg[0] == '-':
            lastarg = arg[1:]
        
        else:
            commands_to_execute.append(arg)

    if len(lastarg) > 0:
        print(f'the value of "{lastarg}" has not been set')


def main():
    global commands_to_execute

    __load_args()

    if params['params_files']: 
        __load_file(params['params_files'])

    __load_args()

    commands_to_execute = list(dict.fromkeys(commands_to_execute)) # removes duplicates

    first = False
    was_command = False
    for command in commands_to_execute:
        if command in commands:
            commands[command]()
            was_command = True
        elif first:
            print(f'unknown command "{command}"')
        else:
            first = True


    if not was_command:
        print('no known commands to execute.\n')
        __help()


commands_to_execute = []

commands = {
    'download-gitlab-commit': lambda: gitlab.repository_downloader(
            params['gitlab_project_id'],
            params['gitlab_access_token']
        ).download_commit(params['path'], params['commit_sha']),
    'download-gitlab-commits': lambda: gitlab.repository_downloader(
            params['gitlab_project_id'],
            params['gitlab_access_token']
        ).download(
            params['path'],
            params['gitlab_all_commits'],
            params['since'],
            params['until']
        ),
    'download-github-newest-commit': lambda: github.download_newes_commit(params['github_access_token'], 
        params['github_repository'], params['path']),
    'download-github-commit': lambda: github.download_commit(params['github_access_token'],
        params['github_repository'], params['path'], params['commit_sha']),
    'download-github-commits': lambda: github.download_commits(params['github_access_token'], 
        params['github_repository'], params['path'], params['since'], params['until']),
    'list-params': __list_params,
    'help': __help
}

params = {
    'params_files': 'GitAPI/params.json',
    'gitlab_project_id': None,
    'github_repository': None,
    'since': None,
    'until': None,
    'path': None,
    'commit_sha': None,
    'gitlab_access_token': None,
    'github_access_token': None,
    'gitlab_all_commits': False 
}

def __init__(commands, parameters):
    global commands_to_execute 
    commands_to_execute = commands
    for key in parameters:
        params[key] = parameters[key]
    
    commands_to_execute = list(dict.fromkeys(commands_to_execute))

    for command in commands_to_execute:
        if command in commands:
            commands[command]()


    if len(commands_to_execute) <= 1:
        raise GitAPIException("no commands to execute, see available command and params")
    
def download_github_repository(token = None, repository_url = None, base_dir = None):
    """Downloads a repository from GitHub

    Args:
        github_token (str, optional): Personalized github token (for further information see https://docs.github.com/en/github/authenticating-to-github/keeping-your-account-and-data-secure/creating-a-personal-access-token). If not set, loaded from params.json in GitAPI directory.
        repository (str, optional): Repository for downloading in following format: Repository owner/Repository. If not set, loaded from params.json in GitAPI directory.
        base_dir (str, optional): Path where you want to dowload the repository. If not set, loaded from params.json in GitAPI directory.
    """
    if params['params_files']: 
        __load_file(params['params_files'])
    
    if not token:
        token = params['github_access_token']
    if not repository_url:
        repository_url = params['github_repository']
    if not base_dir:
        base_dir = params['path']

    if not token or not repository_url or not base_dir:
        raise Exception("No parameters for downloading git hub repository")
    else:
        github.download_newes_commit(token, repository_url, base_dir)

def download_gitlab_repository(project_id:str=None, path:str=None, access_token:str=None, all_commits:bool=None, since:str=None, until:str=None):
    '''Downloads all available commits from a GitLab repository
    
    Args:
        project_id (str, optional): GitLab project ID. If not set, loaded from params.json in GitAPI directory.
        path (str, optional): target download path. If not set, loaded from params.json in GitAPI directory.
        access_token (str, optional): GitLab access token. If not set, loaded from params.json in GitAPI directory.
        download_all_commits (bool, optional): if True, downloads all commits (not only those present on the master branch). If not set, loaded from params.json in GitAPI directory. Defaults to False.
        since (str, optional): only commits committed since this date will be downloaded. Date formats: YYYY-MM-DD, YYYY-MM-DDTHH:MM, YYYY-MM-DDTHH:MM[+-]ZZZZ. If not set, loaded from params.json in GitAPI directory.
        until (str, optional): only commits committed until this date will be downloaded. Date formats: YYYY-MM-DD, YYYY-MM-DDTHH:MM, YYYY-MM-DDTHH:MM[+-]ZZZZ. If not set, loaded from params.json in GitAPI directory.
    '''

    if params['params_files']:
        __load_file(params['params_files'])

    if project_id == None and params['gitlab_project_id']: project_id = params['gitlab_project_id']
    if path == None and params['path']: path = params['path']
    if access_token == None and params['gitlab_access_token']: access_token = params['gitlab_access_token']
    if all_commits == None and params['gitlab_all_commits']: all_commits = params['gitlab_all_commits']
    if since == None and params['since']: since = params['since']
    if until == None and params['until']: until = params['until']

    if project_id == None: raise GitAPIException('download_gitlab_repository: GitLab project ID has not been set')
    if path == None: raise GitAPIException('download_gitlab_repository: the target path has not been set')
    if all_commits == None: all_commits = False

    gitlab.repository_downloader(
        project_id, access_token
    ).download(download_path=path, download_all_commits=all_commits, since=since, until=until)

def download_gitlab_repository_single_commit(project_id:str=None, path:str=None, access_token:str=None, sha:str=None):
    '''Downloads all available commits from a GitLab repository
    
    Args:
        project_id (str, optional): GitLab project ID. If not set, loaded from params.json in GitAPI directory.
        path (str, optional): target download path. If not set, loaded from params.json in GitAPI directory.
        access_token (str, optional): GitLab access token. If not set, loaded from params.json in GitAPI directory.
        sha (str, optional): commit sha. If not set, loaded from params.json in GitAPI directory.
    '''

    if params['params_files']:
        __load_file(params['params_files'])

    if project_id == None and params['gitlab_project_id']: project_id = params['gitlab_project_id']
    if path == None and params['path']: path = params['path']
    if access_token == None and params['gitlab_access_token']: access_token = params['gitlab_access_token']
    if sha == None and params['commit_sha']: sha = params['commit_sha']

    if project_id == None: raise GitAPIException('download_gitlab_repository_single_commit: GitLab project ID has not been set')
    if path == None: raise GitAPIException('download_gitlab_repository_single_commit: the target path has not been set')
    if sha == None: raise GitAPIException('download_gitlab_repository_single_commit: the commit sha has not been set')

    gitlab.repository_downloader(
        project_id,
        access_token
    ).download_commit(path, sha)

if __name__ == '__main__':
    main()
