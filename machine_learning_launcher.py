
# Copyright (C) 2021, Krystof Matejka, Ivan Kraus
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import machine_learning.neural_network as network_utitlities
import parsing.file_parsing

#Instance of a parser. Can be edited to set up the parser settings.
parser = parsing.file_parsing.file_parsing_setup(antlr4shellcommand="java org.antlr.v4.Tool")

def __load_files(input_path):
    """Loads files for machine learning input. The subdirectories must be created like this:
        (input_path) Data ---> True  --> some_file.c
                          |          --> different_file.c
                          |
                          ---> False --> another_file.c
        Note: the filename extantion depaneds on set up of the parser.
        Where True/False directory decides output of the file.

    Args:
        input_path (str): Path to the main directory containing True and False directory.

    Returns:
        two list of str: A list of file names in the True directory and a list of file names in the False directory.
    """
    directories = os.listdir(input_path)
    trues = []
    falses = []
    for dir in directories:
        if dir == "True" or dir == "true":
            dir = os.path.join(input_path, dir)
            for file in os.listdir(dir):
                file = os.path.join(dir, file)
                trues.append(file)
        if dir == "False" or dir == "false":
            dir = os.path.join(input_path, dir)
            for file in os.listdir(dir):
                file = os.path.join(dir, file)
                falses.append(file)
    return trues, falses

def __prepare_data(trues, falses):
    """Combines the files with true and false output and creates a list with their values.

    Args:
        trues (list of str): Containes all files with true as output.
        falses (list of str): Containes all files with false as output.

    Returns:
        two list: First list of str containes merged trues and falses. Second list of bool contains the value of the element of the first list at the same index.
    """
    combined = []
    booleans = []
    
    for file in trues:
        booleans.append(True)
        combined.append(__convert_file(file))

    for file in falses:
        booleans.append(False)
        combined.append(__convert_file(file))

    return combined, booleans

def __convert_file(file_path):
    """Converts files into an AST (Abstract syntax tree) string using the parser.

    Args:
        file_path (str): Path of the file that is converted.

    Returns:
        str: AST string.
    """
    return parser.get_string_parse_tree(file_path)

def create_network(input_path):
    """Creates a trained neural network model based on the training data input specified by the input_path.
       To properly set the input data the files must be placed in following subdirectories.
       (input_path) Data ---> True  --> some_file.c
                         |          --> different_file.c
                         |
                         ---> False --> another_file.c
        Note: the filename extantion depaneds on set up of the parser.
        Where True/False directory decides output of the file.

    Args:
        input_path (str): Path to the main directory containing True and False directory.

    Returns:
        tensorflow.keras.Model: Trained neural network model. 
    """
    trues, falses = __load_files(input_path)
    combined, booleans = __prepare_data(trues, falses)
    model = network_utitlities.train_model(combined, booleans)
    return model

def predict(model_network, file_path):
    """Predicts an output of a specified file.

    Args:
        model_network (tensorflow.keras.Model): A neural network model aquired by training a neural network.
        file_path (str): Path to the file that is used as an input for the neural network.

    Returns:
        int: Most probable output prediction.
    """
    return network_utitlities.predict_model(__convert_file(file_path), model_network)

def save_network(input_path, save_path):
    """Creates, trains and saves a neural network model using check points.
       To properly set the input data the files must be placed in following subdirectories.
       (input_path) Data ---> True  --> some_file.c
                         |          --> different_file.c
                         |
                         ---> False --> another_file.c
        Note: the filename extantion depaneds on set up of the parser.
        Where True/False directory decides output of the file.

    Args:
        input_path (str): Path to the main directory containing True and False directory.
        save_path (str): Path to a directory, that is used to save the chcekpoints.

    Returns:
        tensorflow.keras.Model: Trained neural network model.
    """
    trues, falses = __load_files(input_path)
    combined, booleans = __prepare_data(trues, falses)
    model = network_utitlities.save_model(combined, booleans, save_path)
    return model

def load(load_path):
    """Creats a model and loads checkpoins specified by the load_path.

    Args:
        load_path (str): Path to a directory, that contains saved chcekpoints.

    Returns:
        tensorflow.keras.Model: Trained neural network model.
    """
    model = network_utitlities.load_model(load_path)
    return model
