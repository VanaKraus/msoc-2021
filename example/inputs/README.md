# [arrays directory](arrays)

Data for training property: possibly trying to access an element of some array at the index that is out of bounds

categories of benchmarks in SV-COMP: c/ReachSafety-Arrays, c/MemSafety-Arrays

# [heap directory](heap)

Data for training property: possible dereference of a null pointer (when accessing item of a dynamically allocated structure on heap)

categories of benchmarks in SV-COMP: c/ReachSafety-Heap, c/MemSafety-Heap

# [loops directory](loops)

Data for training property: every loop has provably finite number of iterations

categories of benchmarks in SV-COMP: c/ReachSafety-Loops
