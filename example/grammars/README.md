# Grammars

Grammars used in this example are copied from the [antlr github](https://github.com/antlr/grammars-v4). They have been processed using antlr 4.9.2 afterwards.

For more information see their [github page](https://github.com/antlr/grammars-v4) or documentation to the [parsing module](parsing).