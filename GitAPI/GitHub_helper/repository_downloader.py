
# Copyright (C) 2021, Krystof Matejka, Ivan Kraus
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import requests
from github import Github
import os
import GitAPI.utilities as utilities


#Downloading repository using PyGithub (newest commit)
def __download_directory(repository, download_directory_path, directory_path = ""):
    """Downloads all elements from a Github directory.

    Args:
        repository (github.Repository.Repository): Repository in which we are downloading the files.
        download_directory_path (str): Path to the place at where we are downloading elements.
        directory_path (str, optional): Directory in the Github Repository from which we are downloading. Defaults to "" (which is root).
    """
    for item in repository.get_contents(directory_path):
        #downloads a file
        if item.type == "file":
            r = requests.get(item.download_url)
            try:
                file = open(utilities.to_absolute_path(os.path.join(download_directory_path, 
                    item.path)), "wb")
                file.write(r.content)
                file.close()
            except:
                print(f"There was a proble with {item.path}")
        #makes directory and then recursivly fills it
        elif item.type == "dir":
            path = os.path.join(download_directory_path, item.path)
            if not os.path.exists(utilities.to_absolute_path(path)):
                os.mkdir(utilities.to_absolute_path(path))
            __download_directory(repository, download_directory_path, item.path)

#Downloading repository using requests (all commits)
def __download_commited_directory(token, url, download_directory_path, commit_ref):
    """Downloads all elements from a Github directory.

    Args:
        token (str): Personal Github API token string
        url (str): url to the Github API repository contents
        download_directory_path (str): Path to the place at where we are downloading elements.
        commit_ref (str): The commit's sha
    """
    params = {"ref": commit_ref}
    headers = {"Authorization": f"token {token}"}
    data = requests.get(url, headers=headers, params=params) if commit_ref \
        else requests.get(url, headers=headers)
    for item in data.json():
        #downloads a file
        if item["type"] == "file":
            r = requests.get(item["download_url"])
            try:
                file = open(utilities.to_absolute_path(os.path.join(download_directory_path,
                    item["path"])), "wb")
                file.write(r.content)
                file.close()
            except:
                print(f"There was a proble with {item['path']}")
        #makes directory and then recursivly fills it
        elif item["type"] == "dir":
            path = os.path.join(download_directory_path, item["path"])
            if not os.path.exists(utilities.to_absolute_path(path)):
                os.mkdir(utilities.to_absolute_path(path))
            __download_commited_directory(token, f"{url}/{item['name']}",
                download_directory_path, commit_ref)

#Main function for downlaoding the data
def download_repository(github_token, repository, path, commit_ref = "", create_directory = True):
    """Downloads a Github repository

    Args:
        github_token (str): Personalized github token (for further information see https://docs.github.com/en/github/authenticating-to-github/keeping-your-account-and-data-secure/creating-a-personal-access-token).
        repository (str): Repository for downloading in following format: Repository owner/Repository
        path (str): Path where you want to dowload the repository.
        commit_ref (str, optional): In case of specific commit version include its sha. Defaults to "" for the newest commit.
        create_directory (bool, optional): Creates a directory for the repository. Defaults to True.
    """
    g = Github(github_token)
    repo = g.get_repo(repository)
    
    if create_directory:
        path = os.path.join(path, os.path.basename(os.path.normpath(repository)))
        if not os.path.exists(path):
            os.mkdir(path)
    
    if not commit_ref:
        __download_directory(repo, path)
    else:
        __download_commited_directory(github_token, repo.contents_url[:-8],
            path, commit_ref)
