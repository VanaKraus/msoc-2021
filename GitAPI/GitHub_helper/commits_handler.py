
# Copyright (C) 2021, Krystof Matejka, Ivan Kraus
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from github import Github

#class representing single commit
class Commit:
    """Represents a single commit with its sha and creation date

    Returns:
        Commit: a commit object
    """
    date_format = "%d.%m.%Y %H:%M:%S"

    def __init__(self, commit_sha, commit_date):
        """Initializes a commit

        Args:
            commit_sha (str): Commit's sha string 
            commit_date (datetime.datetime): Commit's creation date
        """
        self.commit_sha = commit_sha
        self.commit_date = commit_date

    def __str__(self):
        """Converts commit to string

        Returns:
            str: Commits creation day and time in a format defined by date_format variable
        """
        return self.commit_date.strftime(self.date_format)

    def get_date(self):
        """Function to get commit's date

        Returns:
            datetime.datetime: datetime when the commit was created
        """
        return self.commit_date
    
    def get_sha(self):
        """Function to get commit's sha

        Returns:
            str: sha of the commit
        """
        return self.commit_sha

def list_commits(github_token, repository, since = None, until = None):
    """Lists all the commits during a certain period of time

    Args:
        github_token (str): Personalized github token (for further information see https://docs.github.com/en/github/authenticating-to-github/keeping-your-account-and-data-secure/creating-a-personal-access-token).
        repository (str): Repository for downloading in following format: Repository owner/Repository
        since (datetime.datetime, optional): The begining of the period of time since when we want to list commits. Defaults to None, which means there is no time limitation for the past.
        until (datetime.datetime, optional): The end of the period of time until when we want to list commits. Defaults to None, which means there is no time limitation for the future.

    Returns:
        Commit[]: Returns a list of commits (of the class Commit)
    """
    github = Github(github_token)
    repo = github.get_repo(repository)
    comms = repo.get_commits()

    #time parameters
    if since and until:
        comms = repo.get_commits(since=since, until=until)
    elif since:
        comms = repo.get_commits(since=since)
    elif until:
        comms = repo.get_commits(until=until)
    
    commits = []
    for commit in comms:
        commits.append(Commit(commit.sha, __get_commit_date(repo, commit.sha)))
    return commits

def __get_commit_date(repository, commit_sha):
    """Gets a commit's creation date

    Args:
        repository (github.Repository.Repository): Base repository of the commit
        commit_sha (str): Commit's sha

    Returns:
        datetime.datetime: Commits's creation datetime
    """
    commit = repository.get_git_commit(sha=commit_sha)
    return commit.committer.date
    
