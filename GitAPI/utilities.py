
# Copyright (C) 2021, Krystof Matejka, Ivan Kraus
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import datetime
import pytz
import re
import os


def prepare_ploads(dict:dict) -> dict:
    '''Removes any key with a None or an empty string value from 'dict'.

    Args:
        dict (dict): the dictionary to be filtered
    
    Returns:
        dict: a filtered dictionary
    '''
    return { key: dict[key] for key in dict if not dict[key] == None or not dict[key] == '' }

def parse_date(date:str, force_offset_aware:bool=False) -> datetime.datetime:
    '''Parses a date string using the appropriate date format.

    Formats implemented:
        %Y-%m-%d
        %Y-%m-%dT%H:%M:%S
        %Y-%m-%dT%H:%M:%S%z

    Args:
        date (str): a date string to be parsed
        force_offset_aware (bool, optional): make offset-aware using pytz.utc.localize if the result is offset-naive. Defaults to False.

    Returns:
        datetime.datetime: the parsed date
    '''

    format = r'%Y-%m-%dT%H:%M:%S%z'

    if not 'T' in date:
        format = r'%Y-%m-%d'
    elif len(re.findall(r'[\+\-]([0-9]{2}:[0-9]{2}|[0-9]{4})', date)) == 0:
        format = r'%Y-%m-%dT%H:%M:%S'

    date = re.sub(r'\.[0-9]+', r'', date)

    res = datetime.datetime.strptime(date, format)

    # if force_offset_aware and is_naive:
    if force_offset_aware and (res.tzinfo is None or res.tzinfo.utcoffset(res) is None):
        res = pytz.utc.localize(res)

    return res

def format_date(date:datetime.datetime, format:str=r'%Y-%m-%dT%H:%M:%S%z') -> str:
    '''Alias for datetime.datetime.strftime(date, format).

    Args:
        date (datetime.datetime): a datetime object
        format (str, optional): the format. Defaults to '%Y-%m-%dT%H:%M:%S%z', which is used by GitLab.
    
    Returns:
        str: formatted date
    '''
    return datetime.datetime.strftime(date, format)

#For windows; MAX_PATH is 260 (including the \0 character)
def __winapi_path(dos_path, encoding=None):
    """Converts a normal path into a length extended path

    Args:
        dos_path (str): path string
        encoding (str, optional): dos_path encoding. Defaults to None, which is unicode encoding.

    Returns:
        str: Absolute length extended path
    """
    if (not isinstance(dos_path, str) and 
        encoding is not None):
        dos_path = dos_path.decode(encoding)
    path = os.path.abspath(dos_path)
    if path.startswith("\\\\"):
        return "\\\\?\\UNC\\" + path[2:]
    return "\\\\?\\" + path

#Helper function for application to run on multiple operating systems
def to_absolute_path(path):
    """Converts path to absolute path and to length extended path (for Windows).

    Args:
        path (str): Path 

    Returns:
        str: Absolute path and absolute length extended path (for Windows)
    """
    if os.name == "nt":
        return __winapi_path(path)
    return os.path.abspath(path)
