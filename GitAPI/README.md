# GitAPI

This module is capable of downloading repositories from GitHub and GitLab. For one-time use, ```GitAPI-launcher.py``` can be called. Its parameters specified in params.json.