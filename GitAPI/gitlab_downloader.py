
# Copyright (C) 2021, Krystof Matejka, Ivan Kraus
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from json.decoder import JSONDecodeError
import os
import json
import requests
import tarfile
import datetime
from pytz import utc

import GitAPI.utilities as utilities

class GitLabDownloaderException(Exception):
    pass

class repository_downloader:
    '''Used to set up an environment to download GitLab repositories.
    
    Methods:
        download(): downloads the repository using given parameters.

    Variables:
        project_id (str): GitLab project ID
        access_token (str, optional): GitLab access token
    '''

    project_id:str
    access_token:str=None

    __commitlist: list

    def __init__(self, project_id:str, access_token:str=None) -> None:
        self.project_id = project_id
        self.access_token = access_token


    def download_commit(self, download_path:str, sha:str):
        '''Downloads a single commit.

        Args:
            download_path (str): target path
            sha (str): sha of the commit
        '''

        print(f'downloading commit {sha}')

        ploads = utilities.prepare_ploads({'access_token': self.access_token, 'sha': sha})
        request = requests.get(f'https://gitlab.com/api/v4/projects/{self.project_id}/repository/archive', params=ploads)

        targetarchivepath = f'{download_path}/~archive.tar.gz'

        with open(os.path.abspath(targetarchivepath), 'wb') as filestream:
            filestream.write(request.content)
            filestream.close()

            try:
                tar = tarfile.open(targetarchivepath, 'r')
                tar.extractall(path=download_path)
                tar.close()
                os.remove(targetarchivepath)
            except tarfile.ReadError as e:
                print(f'tarfile.ReadError at {targetarchivepath}: {e}')
                
                try:
                    reqjson = json.loads(request.content)
                    if 'message' in reqjson:
                        print(reqjson['message'])
                    else:
                        print(reqjson)
                        
                    return
                except:
                    pass

        print('\tDONE')

    def download(self, download_path:str, download_all_commits:bool=False, since:str=None, until:str=None):
        '''Downloads all commits from the repository using given parameters.
        
        Args:
            download_path (str): target download path
            download_all_commits (bool, optional): if True, downloads all commits (not only those present on the master branch)
            since (str, optional): only commits committed since this date will be downloaded. Date formats: YYYY-MM-DD, YYYY-MM-DDTHH:MM, YYYY-MM-DDTHH:MM[+-]ZZZZ
            until (str, optional): only commits committed until this date will be downloaded. Date formats: YYYY-MM-DD, YYYY-MM-DDTHH:MM, YYYY-MM-DDTHH:MM[+-]ZZZZ
        '''

        print('fetching commits')

        manualfiltering = False

        try:
            self.__commitlist = self.__get_commit_list(download_all_commits=download_all_commits, since=since, until=until)
        except RuntimeError as rerr:
            print(rerr)
            print('trying once again')

            try:
                self.__commitlist = self.__get_commit_list(download_all_commits=download_all_commits, since=since, until=until, manualfiltering=True)
                manualfiltering = True
            except Exception as e:
                print(e)
                return

        print()
        print(f'{len(self.__commitlist)} commits fetched')

        if manualfiltering:
            self.__manual_filter(since=since, until=until)

        print(f'{len(self.__commitlist)} commits to download')

        for commit in self.__commitlist:
            #_download_path = f'{download_path}/commit_{commit["short_id"]}'
            _download_path = download_path

            try:
                if not os.path.exists(utilities.to_absolute_path(_download_path)):
                    os.makedirs(utilities.to_absolute_path(_download_path))

                self.download_commit(_download_path, commit['short_id'])
                
            except Exception as e:
                print(e)

    def __get_commit_list(self, download_all_commits:bool, since:str=None, until:str=None, manualfiltering:bool=False) -> list:
        res = []
        page = 0
        while True:
            ploads = { 'access_token': self.access_token, 'per_page': 100, 'page': page, 'all': download_all_commits }

            if not manualfiltering:
                ploads['since'] = since
                ploads['until'] = until

            commitreqploads = utilities.prepare_ploads(ploads)
            commitreq = requests.get(f'https://gitlab.com/api/v4/projects/{self.project_id}/repository/commits', params=commitreqploads)

            try:
                commits = json.loads(commitreq.content)
            except JSONDecodeError as e:
                raise GitLabDownloaderException(f'Error while communicating with the server.\n{e}')

            if 'message' in commits:
                raise RuntimeError(commits['message'])

            if len(commits) == 0:
                break

            res += commits
            page += 1

        return res

    def __manual_filter(self, since:str=None, until:str=None):
        since = utilities.parse_date(since, force_offset_aware=True) if not since == None else datetime.datetime(1970, 1, 1)
        until = utilities.parse_date(until, force_offset_aware=True) if not until == None else datetime.datetime.now(tz=utc)

        for i in range(len(self.__commitlist) - 1, -1, -1):
            commit = self.__commitlist[i]
            date = utilities.parse_date(commit['committed_date'], force_offset_aware=True)

            if date < since or date > until:
                del self.__commitlist[i]
