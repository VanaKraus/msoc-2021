
# Copyright (C) 2021, Krystof Matejka, Ivan Kraus
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import GitAPI.GitHub_helper.repository_downloader as downloader
import GitAPI.GitHub_helper.commits_handler as commitser
import os

def download_newes_commit(token, repository_url, base_dir):
    """Downloads the newest commit from GitHub

    Args:
        github_token (str): Personalized github token (for further information see https://docs.github.com/en/github/authenticating-to-github/keeping-your-account-and-data-secure/creating-a-personal-access-token).
        repository (str): Repository for downloading in following format: Repository owner/Repository
        base_dir (str): Path where you want to dowload the repository.
    """
    if token and repository_url and base_dir:
        try:
            print("Downloading, this might take a while")
            downloader.download_repository(token, repository_url, base_dir)
            print("Downloading finished")
        except:
            print("Error with downloading the directory")
    else:
        print('Parameters inputed incorrectly, please input at least "github_access_token", "github_repository" and "path"')

def download_commit(token, repository_url, base_dir, commit_sha):
    """Downloads a Github repository

        Args:
            token (str): Personalized github token (for further information see https://docs.github.com/en/github/authenticating-to-github/keeping-your-account-and-data-secure/creating-a-personal-access-token).
            repository_url (str): Repository for downloading in following format: Repository owner/Repository
            base_dir (str): Path where you want to dowload the repository.
            commit_sha (str, optional): In case of specific commit version include its sha. Defaults to "" for the newest commit.
        """
    if token and repository_url and base_dir and commit_sha: 
        try:
            print("Downloading, this might take a while")
            downloader.download_repository(token, repository_url, base_dir, commit_sha)
            print("Downloading finished")
        except:
            print("Error with downloading the directory")
    else:
        print('Parameters inputed incorrectly, please input at least "github_access_token", "github_repository", "path" and "commit_sha"')

def download_commits(token, repository_url, base_dir, since = None, until = None):
    """Downloads the newest commit from GitHub

    Args:
        github_token (str): Personalized github token (for further information see https://docs.github.com/en/github/authenticating-to-github/keeping-your-account-and-data-secure/creating-a-personal-access-token).
        repository (str): Repository for downloading in following format: Repository owner/Repository
        base_dir (str): Path where you want to dowload the repository.
        since (datetime.datetime, optional): The begining of the period of time since when we want to list commits. Defaults to None, which means there is no time limitation for the past.
        until (datetime.datetime, optional): The end of the period of time until when we want to list commits. Defaults to None, which means there is no time limitation for the future.
    """
    if token and repository_url and base_dir:
        try:
            print("Downloading, this might take a while")
            for commit in commitser.list_commits(token, repository_url, since, until):
                path = os.path.join(base_dir, str(os.path.basename(os.path.normpath(repository_url))) 
                + "-" + commit.get_sha()[:7])
                if not os.path.exists(path):
                    os.mkdir(path)
                downloader.download_repository(token, repository_url, path, commit.get_sha(), False)
            print("Downloading finished")
        except:
            print("Error with downloading the directory")
    else:
        print('Parameters inputed incorrectly, please input at least "github_access_token", "github_repository" and "path"')
