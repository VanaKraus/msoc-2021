
# Copyright (C) 2021, Krystof Matejka, Ivan Kraus
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import machine_learning_launcher as neural_network
import GitAPI_launcher as downloader
import os

"""
!   !   !   !   !   !   !   !   !   !!! DISCLAIMER !!!  !   !   !   !   !   !   !   !   !
Unfortunately due to the fact, that TextVectorization in machine_learning/neural_network.py
is not working. The final network cannot be created, tested, and used. Therefore before
inplementing into a project the TextVectorization must be fixed. 
Note: We are working on fixing this problem.
!   !   !   !   !   !   !   !   !   !!! DISCLAIMER !!!  !   !   !   !   !   !   !   !   !
"""


def example_one():
    download_path = "example/output/downloaded_repositories"

    #creates a trained neural network to find a property defined by arrays input (see inputs/README.md)
    model = neural_network.create_network("example/inputs/arrays")

    #downloads a repository from github
    downloader.download_github_repository(repository_url = "fffaraz/inSecure-SHell", 
        base_dir = download_path)

    #predicts a property on a downloaded file
    prediction = neural_network.predict(model, os.path.join(download_path, "client/select-tls/main.c"))
    print(prediction)

def example_two():
    download_path = "example/output/downloaded_repositories/cryptsetup"
    neural_network_save_path = "example/output/heap_trained_network"

    #creates and saves a trained neural network to find a property defined by heap input (see inputs/README.md)
    model_trained = neural_network.save_network("example/inputs/heap", neural_network_save_path)
    
    #downloads a repository from gitlab
    downloader.download_gitlab_repository_single_commit(path=download_path, project_id='195655', sha='f6fb5301')
    
    #predicts a property on a downloaded file
    prediction = neural_network.predict(model_trained, os.path.join(download_path, "cryptsetup-f6fb5301-f6fb53012179cb8dbd67f1ba159df8d8d837b4e0/tests/differ.c"))
    print(prediction)

    #loads a saved trained neural network
    model_loaded = neural_network.load(neural_network_save_path)

    #predicts a property on a downloaded file
    prediction = neural_network.predict(model_loaded, os.path.join(download_path, "cryptsetup-f6fb5301-f6fb53012179cb8dbd67f1ba159df8d8d837b4e0/tests/differ.c"))
    print(prediction)


if __name__ == "__main__":
    example_one()
    example_two()
