# MSoC 2021

This project was created within the Matfyz Summer of Code program. Its aim is to use machine learning to test various source codes for given property and to prepare such source codes for this purpose.

It is thus separated into multiple modules (with their own respective READMEs):
- [**GitAPI**](GitAPI) is capable of downloading repositories from GitHub and GitLab,
- [**parsing**](parsing) is capable of parsing source files using [ANTLR v4](https://www.antlr.org/),
- [**machine_learning**](machine_learning) uses source files parsed by parsing to train neural networks to recognise given properties and predict their existence.
- [**sv_comp_benchmarks_inprep**](sv_comp_benchmarks_inprep) prepares training data from the [SV-COMP benchmark](https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks) for model training.

This project was created using Python 3.9. It is (except for grammars and input files included in the example directory) licensed under [APACHE LICENSE, VERSION 2.0](http://www.apache.org/licenses/LICENSE-2.0).

## Required packages

- ```antlr-ast (0.8.1)```
- ```antlr4-python3-runtime (4.9.2)``` (4.7.2 comes with ```antlr-ast```, however 4.9.2 also seems to work fine)
- ```tensorflow (2.6.0)```
- ```pyyaml (6.0)```

## Required installations

Full functionality of the parsing module requires the installation of [ANTLR v4](https://www.antlr.org/). For more information see [parsing](parsing).

## Quick start

```launcher.py``` contains an example use of packages included in this project. It downloads a repository, trains a neural network using provided training data, saves and loads the trained model and predicts the existence of a given property on a file from the repository.

## Limitations

The neural network's text vectorization founded in [```neural_network.py```](machine_learning/neural_network.py) isn't set up correctly. Therefore, before inplementing into a project, the text vectorization must be done. After that, the input neural network shape must be change depending on the text vectorization output shape. We are currently in process of fixing it.

## Future work

In future we are planing on increasing the input samples for the neural network, so it can be better trained. This will increase the accuracy of the neural network.

We are also planing on inproving the neural network model, to better succede in the task of recognizing certain properties.   
