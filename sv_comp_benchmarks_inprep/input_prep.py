
# Copyright (C) 2021, Krystof Matejka, Ivan Kraus
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import shutil
from typing import Iterable
from pathlib import Path

import yaml

class source_file_info:
    '''Contains information about a source file. This information is intended to follow a structure similar to the one given in SV-COMP benchmarks .yml/.yaml files.
    
    Vars:
        input_files (Iterable[str]): source files described by this info.
        properties (dict): properties for which to look in source files. Property names are determined by the names of linked property files.
        options (dict): options of source files.

    Methods:
        __init__(_input_files:Iterable[str], _properties:dict, _options:dict=None): initializes an object.
        @classmethod from_yaml(yaml_object:dict, source_dir:str): creates an instance of source_file_info from a .yml/.yaml file.
    '''

    input_files:Iterable[str]
    properties:dict
    options:dict

    def __init__(self, _input_files:Iterable[str], _properties:dict, _options:dict=None):
        '''Initializes an object.

        Args:
            _input_files (Iterable[str]): paths of source files described by this info.
            _properties (dict): properties for which to look in source files.
            _options (dict, optional): options of source files. None by default.
        '''

        self.input_files, self.properties, self.options = _input_files, _properties, _options
        
    @classmethod
    def from_yaml(cls, yaml_object:dict, source_dir:str):
        '''Creates an instance of source_file_info from a .yml/.yaml file.
        
        Args:
            yaml_object (dict): parsed .yml/.yaml file.
            source_dir (str): the directory where the .yml/.yaml file is located.

        Returns:
            source_file_info: a source_file_info instance.
        '''

        if 'input_files' in yaml_object and 'properties' in yaml_object:
            input_files = [yaml_object['input_files']] if isinstance(yaml_object['input_files'], str) else yaml_object['input_files']
            options = yaml_object['options'] if 'options' in yaml_object else None
            return cls([os.path.join(source_dir, file_name) for file_name in input_files], yaml_object['properties'], options)

        raise SorterError('File info given does not contain requiered fields (\'input_files\', \'properties\')')


class sorter:
    '''An environment the purpose of which is to copy source files from various locations (primarily from the SV-COMP benchmark) into one machine-learning-ready directory.
    
    The resulting file structure looks as follows: ~/<property tested>/<expected verdict>/<file name>.

    Methods:
        __init__(target_dir_path:str, properties:Iterable[str]=None, required_options:dict=None): initializes a sorter.
        sort_input_files(source_dir:str, deep:bool=False): sorts source files from a given directory.
    '''

    __properties: Iterable[str]
    __required_options: dict
    __target_dir_path: str

    def __init__(self, target_dir_path:str, properties:Iterable[str]=None, required_options:dict=None):
        '''Initializes a sorter.

        Args:
            target_dir_path (str): the destination directory, into which the source files will be copied.
            properties (Iterable[str], optional): properties to look for in source files. When None, all properties will be processed. When empty, no properties will be processed.
            required_options (dict): allows to set required options in .yml/.yaml files. Only set requirements will be imposed. None by default.
        '''

        self.__target_dir_path, self.__properties, self.__required_options = target_dir_path, properties, required_options

    def sort_input_files(self, source_dir:str, deep:bool=False):
        '''Sorts source files from a given directory.

        Goes through .yml (.yaml) files in a directory and copies their respective source files into a new directory based on the expected verdict of a test on a given property.

        Destination of a source file (referenced in its .yml/.yaml file) is be determined as follows: [target_dir_path]/<property tested>/<expected verdict>/<file name>.

        Args:
            source_dir (str): the source directory.
            deep (bool): whether to look through child directories. False by default.
        '''

        for entry in os.scandir(source_dir):
            if deep and entry.is_dir():
                self.sort_input_files(entry.path, deep=True)

            elif entry.path.lower().endswith(('.yml', '.yaml')) and entry.is_file():
                try:
                    with open(entry.path, 'r') as yaml_stream:
                        info = source_file_info.from_yaml(yaml.safe_load(yaml_stream), source_dir)
                        self.__process_file_info(info)
                except (SorterError, yaml.YAMLError) as e:
                    print(f'Warning {e.__class__}: sort_input_files: (file skipped) {entry.path}: {e}')

    def __process_file_info(self, info:source_file_info):
        '''Handles the source_file_info processing.
        
        Checks if given source files satisfy requirements imposed by the sorter and copies them to their destination if so.

        Args:
            info (source_file_info): information about source files.
        '''

        for file in info.input_files:
            for property in info.properties:
                if 'expected_verdict' in property:
                    property_name = Path(property['property_file']).stem # property name is determined from the name of a linked property file
                    if self.__check_for_property(property_name) and self.__check_for_required_options(info.options):
                        destination_path = os.path.join(self.__target_dir_path, property_name, str(property['expected_verdict']),  os.path.basename(os.path.normpath(file)))
                        Path(os.path.split(destination_path)[0]).mkdir(parents=True, exist_ok=True) # creates the directory if it does not yet exist
                        
                        if os.path.isfile(file):
                            shutil.copy(file, destination_path)
                        elif os.path.isdir(file):
                            shutil.copytree(file, destination_path, dirs_exist_ok=True)

    def __check_for_property(self, property_name:str) -> bool:
        '''Checks if property is looked for by this sorter.'''
        return self.__properties is None or property_name in self.__properties
    
    def __check_for_required_options(self, options:dict) -> bool:
        '''Checks if options of source files (from source_file_info) satisfy the requirements of this sorter.'''
        if self.__required_options is None or len(self.__required_options) == 0: return True
        if options is None: return False

        for option in self.__required_options:
            if not option in options or self.__required_options[option] != options[option]:
                return False
        return True

class SorterError(Exception):
    pass

