# SV-COMP Benchmarks Input Preparation

This module prepares training data from the [SV-COMP benchmark](https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks) for machine learning model training. This involves filtering of source files based on properties tested and other specifications and their copying into a newly directory structure based on tested properties and their expected verdicts.

Information about source files are taken from .yml/.yaml files, where the structure used by SV-COMP is expected. The resulting file structure looks as follows: ```~/<property tested>/<expected verdict>/<file name>```.

## Python packages used:
* ```pyyaml (6.0)```

## Usage example

```python
from sv_comp_benchmarks_inprep.input_prep import sorter

# Sorts all C source files from 'sv-benchmarks' directory and its subdirectories into 'training-data' directory
sorter('training-data', required_options={'language': 'C'}).sort_input_files('sv-benchmarks', deep=True)

# Sorts all C source files on which the 'unreach-call' property can be tested from 'sv-benchmarks/c/array-cav19'
# and 'sv-benchmarks/c/array-crafted' into 'training-data'
msorter = sorter('training-data', properties=['unreach-call'], required_options={'language': 'C'})
msorter.sort_input_files('sv-benchmarks/c/array-cav19')
msorter.sort_input_files('sv-benchmarks/c/array-crafted')
```