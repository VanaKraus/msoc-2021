import subprocess

def application_to_log(file_name, output_file):
    with open(output_file, "w") as outfile:
        subprocess.call(file_name, stdout=outfile)
